﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Note>().HasData(
                new Note { Id = 1, Title = "NoteTItle", Description = "NoteDescription" },
                new Note { Id = 2, Title = "NoteTItle2", Description = "NoteDescription2" },
                new Note { Id = 3, Title = "NoteTItle3", Description = "NoteDescription3" },
                new Note { Id = 4, Title = "NoteTItle4", Description = "NoteDescription4" },
                new Note { Id = 5, Title = "NoteTItle5", Description = "NoteDescription5" }
            );
        }
    }
}
