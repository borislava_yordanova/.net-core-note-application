﻿# Project Title

This is a simple .NET Core Application for Reading, Creating, Updating and Deleting Notes(2.1)

## Getting Started

In order to take this project on your local machine, open a command promp and type the command "git clone https://tbidbdevelopment@dev.azure.com/tbidbdevelopment/TBI.NoteApplication/_git/TBI.NoteApplication" or go to Repos -> Clone -> Clone in VS Code

### Prerequisites

What things you need to install the software and how to install them

```
Visual Studio Community 2017
```
```
Microsoft SQL Server Management Studio
```

### Installing

A step by step series of examples that tell you how to get a development env running

Follow these steps in order to setup the application

```
1. Open the .sln in the cloned repository file
```
```
2. Open appsettings.json file
```
```
3. Change the connection string with your Microsoft SQL Server Management Studio server name
```
```
4. Open Package Manager Console
```
```
5. Write these commands:
```

* **add-migration init**
* **update-database**

```
6. In "Startup Projects" select 'Clapperboard' and run the solution
```

## Running the tests

Explanation how to run the unit tests for this system.

```
1. Open your Test Explorer in Visual Studio 2017
```
```
2. Click on "Run All"
```
