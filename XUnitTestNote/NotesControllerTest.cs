﻿using Moq;
using Xunit;
using System.Linq;
using AutoMapper;
using DataAccessLayer;
using System.Threading.Tasks;
using DataAccessLayer.Models;
using NoteApplication.Mappings;
using Microsoft.AspNetCore.Mvc;
using NoteApplication.ViewModels;
using System.Collections.Generic;
using NoteApplication.Controllers;
using NoteApplication.Repositories;
using Microsoft.EntityFrameworkCore;

namespace XUnitTestNote
{
    public class NotesControllerTest
    {
        private UnitOfWork _unitOfWork;
        private NoteContext _context;
        private DbContextOptions<NoteContext> _options;
        private static MapperConfiguration _config = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile(new AutoMapperProfile());
        });

        public NotesControllerTest()
        {
            var builder = new DbContextOptionsBuilder<NoteContext>();
            builder.UseInMemoryDatabase(databaseName: "NoteDatabaseTest");

            _options = builder.Options;
            _context = new NoteContext(_options);
            _unitOfWork = new UnitOfWork(_context);
        }

        [Fact(DisplayName = "Create Note Success")]
        public async Task TestCreateNoteSuccess()
        {
            //Arrange
            var context = new NoteContext(_options);
            var unitOfWork = new UnitOfWork(context);
            var note = new Note()
            {
                Description = "Note Description",
                Title = "Note Title"
            };

            //Act
            await unitOfWork.NoteRepository.Insert(note);
            await unitOfWork.SaveAsync();

            //Assert
            Assert.NotNull(note);
            Assert.Equal("Note Title", note.Title);
            Assert.Equal("Note Description", note.Description);
        }

        [Fact(DisplayName = "Select Note Success")]
        public async Task TestSelectNoteSuccess()
        {
            //Arrange
            var context = new NoteContext(_options);
            var unitOfWork = new UnitOfWork(context);
            var note = new Note()
            {
                Description = "Note Description",
                Title = "Note Title"
            };

            //Act
            await unitOfWork.NoteRepository.SelectByID(note.Id);
            await unitOfWork.SaveAsync();

            //Assert
            Assert.NotNull(note);
            Assert.Equal("Note Title", note.Title);
            Assert.Equal("Note Description", note.Description);
        }

        [Fact(DisplayName = "Select List Notes Success")]
        public async Task TestSelectListNotesSuccess()
        {
            //Arrange
            var context = new NoteContext(_options);
            var unitOfWork = new UnitOfWork(context);
            var notes = new List<Note>
            {
                new Note() { Id = 1, Description = "Note Description 1", Title = "Note Title 1" },
                new Note() { Id = 2, Description = "Note Description 2", Title = "Note Title 2" },
                new Note() { Id = 3, Description = "Note Description 3", Title = "Note Title 3" }
            };

            //Act
            await unitOfWork.NoteRepository.SelectAll();
            await unitOfWork.SaveAsync();

            //Assert
            Assert.NotNull(notes);
            Assert.Equal(3, notes.Count());
            Assert.IsType<List<Note>>(notes);
        }

        [Fact(DisplayName = "Update Note Success")]
        public async Task TestUpdateNoteSuccess()
        {
            //Arrange
            var context = new NoteContext(_options);
            var unitOfWork = new UnitOfWork(context);
            Note note = await unitOfWork.NoteRepository.SelectByID(1);

            note = new Note()
            {
                Title = "Updated Title",
                Description = "Updated Description"
            };

            //Act
            unitOfWork.NoteRepository.Update(note);
            await unitOfWork.SaveAsync();

            //Assert
            Assert.NotNull(note);
            Assert.Equal("Updated Title", note.Title);
            Assert.Equal("Updated Description", note.Description);
        }

        [Fact(DisplayName = "Delete Note Success")]
        public async Task TestDeleteNoteSucess()
        {
            //Arrange
            var context = new NoteContext(_options);
            var unitOfWork = new UnitOfWork(context);
            var note = new Note()
            {
                Title = "Delete Note Title",
                Description = "Delete Note Description"
            };

            //Act
            await unitOfWork.NoteRepository.Insert(note);
            await unitOfWork.SaveAsync();

            unitOfWork.NoteRepository.Delete(note);
            await unitOfWork.SaveAsync();

            var noteDelete = await unitOfWork.NoteRepository.SelectByID(note.Id);

            //Assert
            Assert.Null(noteDelete);
        }

        [Fact(DisplayName = "Mock Unit of Work Select Note Success")]
        public async Task TestMockUnitOfWorkSelectNoteSuccess()
        {
            var note = new Note()
            {
                Id = 3,
                Description = "Note Description 3",
                Title = "Note Title 3"
            };

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(x => x.NoteRepository.SelectByID(3))
                .Returns(Task.FromResult<Note>(note));

            var mapper = _config.CreateMapper();
            var controller = new NotesController(mapper, mockUnitOfWork.Object);

            //Act
            var result = await controller.Details(3);
            var view = result as ViewResult;

            Assert.IsType<ViewResult>(result);
            Assert.NotNull(view);
            Assert.NotNull(view.Model);
            Assert.IsType<NoteViewModel>(view.Model);

            var data = view.Model as NoteViewModel;
            Assert.Equal(note.Id, data.Id);
            Assert.Equal(note.Description, data.Description);
            Assert.Equal(note.Title, data.Title);
        }

        [Fact(DisplayName = "Mock Unit of Work Select List Notes Success")]
        public async Task TestMockUnitOfWorkNoteListSuccess()
        {
            var notes = new Note[]
            {
                new Note() { Id = 1, Description = "Note Description 1", Title = "Note Title 1" },
                new Note() { Id = 2, Description = "Note Description 2", Title = "Note Title 2" },
                new Note() { Id = 3, Description = "Note Description 3", Title = "Note Title 3" }
            };

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(x => x.NoteRepository.SelectAll())
                .Returns(Task.FromResult<IEnumerable<Note>>(notes));

            var mapper = _config.CreateMapper();
            var controller = new NotesController(mapper, mockUnitOfWork.Object);

            //Act
            var result = await controller.Index();
            var view = result as ViewResult;

            Assert.IsType<ViewResult>(result);
            Assert.NotNull(view);
            Assert.NotNull(view.Model);
            Assert.IsType<List<NoteViewModel>>(view.Model);

            var data = view.Model as List<NoteViewModel>;
            for (int i = 0; i < data.Count; i++)
            {
                Assert.Equal(notes[i].Id, data[i].Id);
                Assert.Equal(notes[i].Description, data[i].Description);
                Assert.Equal(notes[i].Title, data[i].Title);
            }
        }

        [Fact(DisplayName = "Mock Unit of Work Create Note Success")]
        public void TestMockUnitOfWorkCreateNoteSuccessAsync()
        {
            // Arrange
            var note = new Note()
            {
                Id = 5,
                Title = "Note Title Create",
                Description = "Note Description Create"
            };

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(x => x.NoteRepository.Insert(note))
                .Returns(Task.FromResult<Note>(note));

            mockUnitOfWork
                .Setup(x => x.SaveAsync())
                .Returns(Task.FromResult<Note>(note));

            var mapper = _config.CreateMapper();
            var controller = new NotesController(mapper, mockUnitOfWork.Object);

            // Act
            var result = controller.Create();
            var view = result as ViewResult;

            // Assert
            Assert.NotNull(view);
            Assert.NotNull(view.Model);
            Assert.IsType<ViewResult>(result);
            Assert.Equal(5, note.Id);
            Assert.Equal("Note Title Create", note.Title);
            Assert.Equal("Note Description Create", note.Description);
        }

        [Fact(DisplayName = "Mock Unit of Work Update Note Success")]
        public async Task TestMockUnitOfWorkUpdateNoteSuccess()
        {
            // Arrange
            var note = new Note()
            {
                Title = "Note Title",
                Description = "Note Description"
            };
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(x => x.NoteRepository.SelectByID(note.Id))
                .Returns(Task.FromResult(note));

            note = new Note()
            {
                Title = "Updated Note Title",
                Description = "Updated Note Description"
            };
            mockUnitOfWork
               .Setup(x => x.NoteRepository.Update(note));
            mockUnitOfWork
               .Setup(x => x.SaveAsync());

            var mapper = _config.CreateMapper();
            var controller = new NotesController(mapper, mockUnitOfWork.Object);

            // Act
            var result = controller.Edit(note.Id);
            var view = await result as ViewResult;

            // Assert
            Assert.NotNull(view);
            Assert.NotNull(view.Model);
            Assert.Equal("Updated Note Title", note.Title);
            Assert.Equal("Updated Note Description", note.Description);
        }

        [Fact(DisplayName = "Mock Mapper Select Note Success")]
        public async Task TestMockMapperNoteSuccess()
        {
            // Arrange
            var noteViewModel = new NoteViewModel();
            var note = new Note()
            {
                Description = "Note Description",
                Title = "Note Title"
            };

            var mockMapper = new Mock<IMapper>();
            mockMapper
                .Setup(m => m.Map<NoteViewModel>(It.IsAny<Note>()))
                .Returns(noteViewModel);

            // Act
            var controller = new NotesController(mockMapper.Object, _unitOfWork);
            var result = await controller.Details(note.Id);
            var view = result as ViewResult;

            // Assert
            Assert.IsType<ViewResult>(result);
            Assert.IsType<NoteViewModel>(view.Model);
            Assert.NotNull(view);
            Assert.NotNull(view.Model);

            var data = view.Model as NoteViewModel;
            Assert.Equal(data.Id, noteViewModel.Id);
            Assert.Equal(data.Title, noteViewModel.Title);
            Assert.Equal(data.Description, noteViewModel.Description);
        }

    }
}
