﻿using DataAccessLayer;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NoteApplication.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        private readonly NoteContext _context;
        private readonly DbSet<T> _table;

        public BaseRepository(NoteContext context)
        {
            this._context = context;
            this._table = _context.Set<T>();
        }

        public async Task<IEnumerable<T>> SelectAll()
        {
            return await _table.ToListAsync();
        }

        public async Task<T> SelectByID(int id)
        {
            return await _table.FindAsync(id);
        }

        public async Task Insert(T obj)
        {
            await _table.AddAsync(obj);
        }

        public void Update(T obj)
        {
            _table.Update(obj);
        }

        public void Delete(T obj)
        {
            _table.Remove(obj);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _context.Dispose();
                }

                disposedValue = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);

        }
        #endregion
    }
}

