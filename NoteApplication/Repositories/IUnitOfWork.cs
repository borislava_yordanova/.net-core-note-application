﻿using System;
using System.Threading.Tasks;
using DataAccessLayer.Models;

namespace NoteApplication.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        IBaseRepository<Note> NoteRepository { get; }
        Task SaveAsync();
    }
}
