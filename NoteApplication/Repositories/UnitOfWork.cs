﻿using System;
using DataAccessLayer;
using DataAccessLayer.Models;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;


namespace NoteApplication.Repositories
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly NoteContext _context;
        private IBaseRepository<Note> _modelRepository;

        public UnitOfWork(NoteContext context)
        {
            _context = context;
        }

        public IBaseRepository<Note> NoteRepository
        {
            get
            {
                if (_modelRepository == null)
                {
                    _modelRepository = new BaseRepository<Note>(_context);
                }
                return _modelRepository;
            }
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }
    }
}
