﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace NoteApplication.Repositories
{
    public interface IBaseRepository<T> : IDisposable 
    {
        Task<IEnumerable<T>> SelectAll();
        Task<T> SelectByID(int id);
        Task Insert(T obj);
        void Update(T obj);
        void Delete(T obj);
    }
}
