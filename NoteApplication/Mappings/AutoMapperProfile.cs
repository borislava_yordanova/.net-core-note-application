﻿using AutoMapper;
using DataAccessLayer.Models;
using NoteApplication.ViewModels;

namespace NoteApplication.Mappings
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Note, NoteViewModel>();
            CreateMap<NoteViewModel, Note>();
        }
    }
}
