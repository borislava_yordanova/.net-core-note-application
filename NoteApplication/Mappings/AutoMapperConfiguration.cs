﻿using AutoMapper;
using DataAccessLayer.Models;
using NoteApplication.ViewModels;

namespace NoteApplication.Mappings
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<Note, NoteViewModel>();
                cfg.CreateMap<NoteViewModel, Note>();    
            });
        }
    }
}
