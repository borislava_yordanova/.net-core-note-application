﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http.Description;
using AutoMapper;
using DataAccessLayer.Models;
using Microsoft.AspNetCore.Mvc;
using NoteApplication.Repositories;
using NoteApplication.ViewModels;

namespace NoteApplication.Controllers
{
    public class NotesController : Controller
    {  
        private IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public NotesController(IMapper mapper, IUnitOfWork unitOfWork )
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        [HttpGet]
        [Route("notes/")]
        [ResponseType(typeof(NoteViewModel))]
        public async Task<IActionResult> Index()
        {
            var notes = await _unitOfWork.NoteRepository.SelectAll();
            var notesViewModel = _mapper.Map<List<NoteViewModel>>(notes);
            return View(notesViewModel);
        }

        [Route("notes/{id}")]
        public async Task<IActionResult> Details(int? id)
        {
            var note = await _unitOfWork.NoteRepository.SelectByID(id.Value);
            var noteViewModel = _mapper.Map<NoteViewModel>(note);
            return View(noteViewModel);
        }

        [Route("notes/create")]
        public IActionResult Create()
        {
            var model = new NoteViewModel();
            return View(model);
        }

        [Route("notes/create")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateConfirmed(NoteViewModel noteViewModel)
        {
            var note = new Note();
            var newNote = _mapper.Map(noteViewModel, note);
            await _unitOfWork.NoteRepository.Insert(newNote);
            await _unitOfWork.SaveAsync();       
            return RedirectToAction("Index");
        }

        [Route("notes/edit/{id}")]
        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            var note = await _unitOfWork.NoteRepository.SelectByID(id.Value);
            var noteViewModel = new NoteViewModel();
            var editedNote = _mapper.Map(note, noteViewModel);
            return View(editedNote);
        }

        [Route("notes/edit/{id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditConfirmed(NoteViewModel noteViewModel)
        {
            var note = new Note();
            var newNote = _mapper.Map(noteViewModel, note);
            _unitOfWork.NoteRepository.Update(newNote);
            await _unitOfWork.SaveAsync();
            return RedirectToAction("Index");
        }

        [Route("notes/delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
            var note = await _unitOfWork.NoteRepository.SelectByID(id.Value);
            var noteViewModel = new NoteViewModel();
            var deletedNote = _mapper.Map(note, noteViewModel);
            return View(deletedNote);
        }

        [HttpPost]
        [Route("notes/delete/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(NoteViewModel noteViewModel, int? id)
        {
            var note = await _unitOfWork.NoteRepository.SelectByID(id.Value);
            var deleteNote = _mapper.Map(noteViewModel, note);
            _unitOfWork.NoteRepository.Delete(deleteNote);
            await _unitOfWork.SaveAsync();

            return RedirectToAction("Index");
        }
    }
}
