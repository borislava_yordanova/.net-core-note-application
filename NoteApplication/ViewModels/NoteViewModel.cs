﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace NoteApplication.ViewModels
{
    public class NoteViewModel
    {
        [ScaffoldColumn(false)]
        [Key]
        public int Id { get; set; }

        [DisplayName("Title")]
        [Required(ErrorMessage = "The note title is required field")]
        [StringLength(20)]
        public string Title { get; set; }

        [DisplayName("Description")]
        [Required(ErrorMessage = "The note description is required field")]
        [StringLength(100)]
        public string Description { get; set; }
    }
}
